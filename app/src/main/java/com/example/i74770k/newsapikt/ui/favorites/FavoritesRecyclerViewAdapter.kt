package com.example.i74770k.newsapikt.ui.favorites

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.example.i74770k.newsapikt.R
import com.example.i74770k.newsapikt.repository.remote.api.model.Article
import com.example.i74770k.newsapikt.repository.remote.api.model.ClickItemRealm
import com.example.i74770k.newsapikt.tools.PagingAdapter
import com.jakewharton.rxbinding2.view.RxView
import inflate
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import isVisible
import kotlinx.android.synthetic.main.recyclerview_item_articles_layout.view.*
import load


/**
 * Created by i7-4770k on 4/3/2018
 */
class FavoritesRecyclerViewAdapter(private var mModels: List<Article>) :
        RecyclerView.Adapter<FavoritesRecyclerViewAdapter.ViewHolder>(), PagingAdapter<Article> {

    val clickSubject = PublishSubject.create<ClickItemRealm>()
    val clickEvent: Observable<ClickItemRealm> = clickSubject

    private var isExpired: Boolean = true

    override fun skipListExpired() {
        if (!isExpired) {
            isExpired = true
            skipList()
        }
    }

    override fun skipList() {
        notifyDataSetChanged()
    }

    override fun updateList(newItems: List<Article>) {
        mModels = newItems
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(parent.inflate(R.layout.recyclerview_item_articles_layout))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mModels[position]
        holder.titleTextView?.apply { text = model.title }
        holder.descriptionTextView?.apply { text = model.description }
        holder.dateTextView?.apply { text = model.publishedAt }
        holder.logoImageView.load(model.urlToImage)
        holder.logoImageView.isVisible = model.urlToImage != null
        rxViewClicks(position, model, holder.parentView, holder.shareImageButton, holder.favoritesImageButton)
    }

    override fun getItemCount(): Int = mModels.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val parentView = view.parentView
        val logoImageView = view.logoImageView
        val titleTextView = view.titleTextView
        val descriptionTextView = view.descriptionTextView
        val dateTextView = view.dateTextView
        val favoritesImageButton = view.favoritesImageButton
        val shareImageButton = view.shareImageButton

    }

    private fun rxViewClicks(position: Int, model: Article, vararg views: View?) {
        for (view in views) {
            view?.let {
                RxView.clicks(view)
                        .map<ClickItemRealm> { ClickItemRealm(view.id, position, model) }
                        .subscribe(clickSubject)
            }
        }
    }

}