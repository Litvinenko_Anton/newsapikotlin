package com.example.i74770k.newsapikt.di

import android.app.Application
import android.content.Context
import com.example.i74770k.newsapikt.App
import com.example.i74770k.newsapikt.BuildConfig
import com.example.i74770k.newsapikt.repository.RepositoryImp
import com.example.i74770k.newsapikt.repository.locale.LocaleRepository
import com.example.i74770k.newsapikt.repository.locale.LocaleRepositoryRealmImp
import com.example.i74770k.newsapikt.repository.remote.RemoteRepository
import com.example.i74770k.newsapikt.repository.remote.api.NewsApi
import com.example.i74770k.newsapikt.repository.remote.api.RemoteRepositoryRetrofitImpl
import com.example.i74770k.newsapikt.repository.remote.api.model.Article
import com.example.i74770k.newsapikt.tools.BASE_URL
import com.example.i74770k.newsapikt.tools.OK_HTTP_CLIENT_TIMEOUT
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Component
import dagger.Module
import dagger.Provides
import io.realm.Realm
import io.realm.RealmConfiguration
import novo.net.novo.di.ApplicationClass
import novo.net.novo.di.ApplicationContext
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, RealmModule::class, ApiModules::class])
interface ApplicationComponent {

    @ApplicationContext
    fun context(): Context

    fun inject(app: App)

    fun inject(app: InjectHelper)

}

@Module
class ApplicationModule(private val context: Context, private val application: Application) {

    @Provides
    @Singleton
    @ApplicationContext
    fun context() = context

    @Provides
    @Singleton
    @ApplicationClass
    fun application() = application

}

@Module
class RealmModule(private val appContext: Context) {

    @Provides
    @Singleton
    internal fun provideRealm(realmConfiguration: RealmConfiguration): Realm {
        Realm.setDefaultConfiguration(realmConfiguration)
        return Realm.getDefaultInstance()
    }

    @Provides
    @Singleton
    internal fun provideRealmConfiguration(): RealmConfiguration {
        Realm.init(appContext)
        return RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build()
    }

    @Provides
    internal fun provideLocaleRepository(realm: Realm): LocaleRepository<Article> {
        return LocaleRepositoryRealmImp(realm)
    }

}
@Module
class ApiModules(private val appContext: Context) {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(logging)
        }

        builder.connectTimeout((OK_HTTP_CLIENT_TIMEOUT * 1000).toLong(), TimeUnit.MILLISECONDS)
                .readTimeout((OK_HTTP_CLIENT_TIMEOUT * 1000).toLong(), TimeUnit.MILLISECONDS)

        return builder.build()
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return gsonBuilder
                .setLenient()
                .create()
    }

    @Provides
    @Singleton
    fun provideRestAdapter(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        val builder = Retrofit.Builder()
        builder.client(okHttpClient)
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
        return builder.build()
    }

    @Provides
    @Singleton
    fun provideApiService(restAdapter: Retrofit): NewsApi {
        return restAdapter.create(NewsApi::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteRepository(apiService: NewsApi): RemoteRepository {
        return RemoteRepositoryRetrofitImpl(appContext, apiService)
    }

    @Provides
    @Singleton
    fun provideRepository(remoteRepository: RemoteRepository, localeRepository:LocaleRepository<Article>): RepositoryImp {
        return RepositoryImp(remoteRepository, localeRepository)
    }

}
