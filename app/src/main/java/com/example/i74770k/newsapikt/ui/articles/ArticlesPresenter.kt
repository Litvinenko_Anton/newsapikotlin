package com.example.i74770k.newsapikt.ui.articles

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.i74770k.newsapikt.presentor.BasePresenter
import com.example.i74770k.newsapikt.repository.locale.LocaleRepositoryRealmImp
import com.example.i74770k.newsapikt.repository.remote.api.model.Article
import com.example.i74770k.newsapikt.repository.remote.api.model.ArticlesRequestModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.ArticleModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.ArticlesModel
import com.example.i74770k.newsapikt.tools.API_KEY
import io.realm.RealmResults

/**
 * Created by i7-4770k on 3/21/2018
 */
@InjectViewState
class ArticlesPresenter : BasePresenter<ArticlesPresenter.View>() {

    private var inputData: ArticlesRequestModel = ArticlesRequestModel()

    private val list = ArrayList<ArticleModel>()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.initAdapter(list)
        viewState.initInputDate(inputData)
        initFavoritesResultsOnChange()
    }

    fun reloadArticles() {
        if (list.size > 0) list.clear()
        loadMoreArticles(1)
    }

    fun loadMoreArticles(page: Int) {
        inputData.page = page
        if (inputData.isAllArticles())
            loadAllArticles(inputData)
        else
            loadTopArticles(inputData)
    }

    private fun loadTopArticles(inputData: ArticlesRequestModel) {
        repository.getArticlesTop(API_KEY, inputData)
                .doOnRequest { showProgress("TopArticles") }
                .subscribe({ response -> initArticlesResponse(response) },
                        { throwable -> onError(throwable, "TopArticles") },
                        { onCompleted("TopArticles") })
    }

    private fun loadAllArticles(inputData: ArticlesRequestModel) {
        repository.getArticles(API_KEY, inputData)
                .doOnRequest { showProgress("AllArticles") }
                .subscribe({ response -> initArticlesResponse(response) },
                        { throwable -> onError(throwable, "AllArticles") },
                        { onCompleted("AllArticles") })
    }

    private fun initArticlesResponse(response: ArticlesModel) {
        if (response.isSuccess())
            viewState.responseComplete(response.isSuccess(), initArticlesList(response))
        else
            viewState.responseError(response.message ?: "No error message")
    }

    private fun initArticlesList(response: ArticlesModel): List<ArticleModel> {
        for (articleModel in response.articles) {
            articleModel.favorites = repository.containsInRealm(articleModel)
            list.add(articleModel)
        }
        return list
    }

    private fun initFavoritesResultsOnChange() {
        repository.addOnResultsChangeListener(object : LocaleRepositoryRealmImp.OnResultsChangeListener {
            override fun resultsOnChange(articles: RealmResults<Article>) {
                for (articleModel in list) {
                    articleModel.favorites = repository.containsInRealm(articleModel)
                }
                viewState.responseComplete(list.isNotEmpty(), list)
            }
        })
    }

    fun initToFavorites(model: ArticleModel) {
        if (model.favorites) {
            repository.deleteFromRealm(model)
        } else {
            repository.saveToRealm(model)
        }
        viewState.responseComplete(list.isNotEmpty(), list)
    }


    /**
     * ViewState
     */

    @StateStrategyType(AddToEndSingleStrategy::class)
    interface View : BasePresenter.View {

        fun initAdapter(list: List<ArticleModel>)

        fun responseComplete(isSuccess: Boolean, list: List<ArticleModel>)

        fun initInputDate(inputData: ArticlesRequestModel?)

    }

}