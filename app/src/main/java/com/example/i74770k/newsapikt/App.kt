package com.example.i74770k.newsapikt

import android.app.Application
import android.content.Context
import com.arellomobile.mvp.MvpFacade
import com.example.i74770k.newsapikt.di.*
import io.paperdb.Paper

/**
 * Created by i7-4770k on 3/21/2018
 */
class App : Application() {

    companion object {
        lateinit var appContext: Context
        lateinit var applicationComponent: ApplicationComponent
    }

    init {
        appContext = this@App
        initApplicationComponent()
        MvpFacade.init()
    }

    override fun onCreate() {
        super.onCreate()
        Paper.init(applicationContext)
    }

    private fun initApplicationComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this@App, this@App))
                .realmModule(RealmModule(this@App))
                .apiModules(ApiModules(this@App))
                .build()
    }

}