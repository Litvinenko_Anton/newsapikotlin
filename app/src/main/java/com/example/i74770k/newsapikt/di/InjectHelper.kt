package com.example.i74770k.newsapikt.di

import com.example.i74770k.newsapikt.App
import com.example.i74770k.newsapikt.repository.RepositoryImp
import javax.inject.Inject

class InjectHelper {

    @Inject
    lateinit var repository: RepositoryImp

    init {
        App.applicationComponent.inject(this)
    }

}