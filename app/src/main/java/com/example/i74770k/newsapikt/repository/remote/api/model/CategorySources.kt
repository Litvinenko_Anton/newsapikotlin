package com.example.i74770k.newsapikt.repository.remote.api.model

import com.example.i74770k.newsapikt.repository.remote.api.pojo.SourceModel

class CategorySources {

    constructor(model: SourceModel){
        category = model.category ?: ""
        categoryName = model.name ?: ""
        sourcesList.add(model)
    }

    var category: String = ""
    var categoryName: String = ""

    val sourcesList:MutableList<SourceModel> = ArrayList()

    fun categoryCount():String = sourcesList.size.toString()

}