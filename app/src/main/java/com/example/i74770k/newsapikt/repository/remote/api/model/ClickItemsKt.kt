package com.example.i74770k.newsapikt.repository.remote.api.model

import com.example.i74770k.newsapikt.repository.remote.api.pojo.ArticleModel

open class ClickItem(val id: Int, val position: Int)

class ClickItemModel(id: Int, position: Int, val model: ArticleModel) : ClickItem(id, position)

class ClickItemRealm(id: Int, position: Int, val model: Article) : ClickItem(id, position)