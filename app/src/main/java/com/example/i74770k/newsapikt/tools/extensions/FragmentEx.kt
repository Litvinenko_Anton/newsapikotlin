package com.example.i74770k.newsapikt.tools.extensions

import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import com.example.i74770k.newsapikt.App
import showToast
import toastD

val Fragment.customApplication: App
    get() = activity?.application as App

fun Fragment.showToast(@StringRes resId: Int) = context?.showToast(resId)

fun Fragment.showToast(message: String?) = context?.showToast(message)

fun Fragment.toastD(message: String?) = context?.toastD(message)
