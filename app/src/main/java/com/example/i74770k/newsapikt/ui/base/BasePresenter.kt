package com.example.i74770k.newsapikt.presentor

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.i74770k.newsapikt.di.InjectHelper
import com.example.i74770k.newsapikt.repository.RepositoryImp
import com.example.i74770k.newsapikt.tools.extensions.logD
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by i7-4770k on 3/21/2018
 */
//@InjectViewState must be disable for BasePresenter
abstract class BasePresenter<view : BasePresenter.View> : MvpPresenter<view>() {

    var repository: RepositoryImp = InjectHelper().repository

    private val disposables = CompositeDisposable()

    fun addDisposable(disposable: Disposable) = disposables.add(disposable)

    override fun onDestroy() {
        disposables.dispose()
        disposables.clear()
        super.onDestroy()
    }

    protected fun showProgress(tag: String) {
        logD("showProgress_$tag")
        viewState.displayProgress(true)
    }

    private fun hideProgress(tag: String) {
        logD("hideProgress $tag")
        viewState.displayProgress(false)
    }

    protected fun onCompleted(tag: String) {
        logD("onCompleted_$tag")
        hideProgress("OnCompleted_$tag")
    }

    protected fun onError(t: Throwable, tag: String) {
        logD("$tag.doOnError() -> " + t.message)
        onError(tag)
    }

    private fun onError(tag: String) {
        logD("onError_$tag")
        hideProgress("OnError_$tag")
    }

    protected fun showResponseError(error: String) {
        logD("showError $error")
        viewState.responseError(error)
    }

    /**
     * ViewState
     */

    @StateStrategyType(AddToEndSingleStrategy::class)
    interface View : MvpView {

        fun displayProgress(inProgress: Boolean)

        fun responseComplete(isSuccess: Boolean)

        fun responseError(errorMessage: String)

    }

}