package com.example.i74770k.newsapikt.ui.favorites

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.i74770k.newsapikt.R
import com.example.i74770k.newsapikt.repository.remote.api.model.Article
import com.example.i74770k.newsapikt.repository.remote.api.model.ClickItemRealm
import com.example.i74770k.newsapikt.tools.extensions.sharedContent
import com.example.i74770k.newsapikt.tools.extensions.showToast
import com.example.i74770k.newsapikt.ui.base.BaseFragment
import inflate
import kotlinx.android.synthetic.main.fragment_favorites_layout.*

/**
 * Created by i7-4770k on 3/21/2018
 */


class FavoritesFragment : BaseFragment(), FavoritesPresenter.View {


    @InjectPresenter
    lateinit var mPresenter: FavoritesPresenter

    companion object {
        val TAG = FavoritesFragment::class.java.simpleName

        fun newInstance(): FavoritesFragment {
            return FavoritesFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_favorites_layout)
    }

    private fun initRecyclerView(list: List<Article>) {
        favoritesRecyclerView?.apply {
            layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
            val categoryAdapter = FavoritesRecyclerViewAdapter(list)
            setupItemClick(categoryAdapter)
            adapter = categoryAdapter
        }
    }

    private fun setupItemClick(adapter: FavoritesRecyclerViewAdapter) {
        subscribe = adapter.clickEvent.subscribe { onItemClick(it) }
    }

    private fun onItemClick(clickItem: ClickItemRealm) {
        when (clickItem.id) {
            R.id.favoritesImageButton -> mPresenter.deleteFromFavorites(clickItem.model)
            R.id.shareImageButton -> sharedContent(context!!, clickItem.model)
            R.id.parentView -> showToast(clickItem.model.title)
        }
    }

    override fun displayProgress(inProgress: Boolean) {

    }

    override fun realmResults(isSuccess: Boolean, list: List<Article>) {
        favoritesRecyclerView?.apply {
            if (adapter == null) {
                initRecyclerView(list)
            } else {
                if (adapter is FavoritesRecyclerViewAdapter)
                    (adapter as FavoritesRecyclerViewAdapter).updateList(list)
            }
        }
    }
}