package com.example.i74770k.newsapikt.repository.remote.api.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Article : RealmObject() {

    @PrimaryKey var id: Int = 0
    var favorites: Boolean = false
    var author: String? = null
    var title: String? = null
    var description: String? = null
    var url: String? = null
    var urlToImage: String? = null
    var publishedAt: String? = null

    fun getPrimaryKey(): Int {
        if (id == 0)
            id = hashCode()
        return id
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Article

        if (id != other.id) return false
        if (favorites != other.favorites) return false
        if (author != other.author) return false
        if (title != other.title) return false
        if (description != other.description) return false
        if (url != other.url) return false
        if (urlToImage != other.urlToImage) return false
        if (publishedAt != other.publishedAt) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + favorites.hashCode()
        result = 31 * result + (author?.hashCode() ?: 0)
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (description?.hashCode() ?: 0)
        result = 31 * result + (url?.hashCode() ?: 0)
        result = 31 * result + (urlToImage?.hashCode() ?: 0)
        result = 31 * result + (publishedAt?.hashCode() ?: 0)
        return result
    }


}