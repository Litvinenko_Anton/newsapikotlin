package com.example.i74770k.newsapikt.ui.articles

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.i74770k.newsapikt.R
import com.example.i74770k.newsapikt.repository.remote.api.model.ArticlesRequestModel
import com.example.i74770k.newsapikt.repository.remote.api.model.CategorySources
import com.example.i74770k.newsapikt.repository.remote.api.model.ClickItemModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.ArticleModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.SourceModel
import com.example.i74770k.newsapikt.tools.*
import com.example.i74770k.newsapikt.tools.extensions.ConvertDateUtil
import com.example.i74770k.newsapikt.tools.extensions.sharedContent
import com.example.i74770k.newsapikt.tools.extensions.showToast
import com.example.i74770k.newsapikt.ui.base.BaseFragment
import com.jakewharton.rxbinding2.widget.RxAdapterView
import com.jakewharton.rxbinding2.widget.RxCompoundButton
import com.jakewharton.rxbinding2.widget.RxRadioGroup
import com.jakewharton.rxbinding2.widget.RxTextView
import inflate
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import isVisible
import kotlinx.android.synthetic.main.fragment_articles_layout.*
import kotlinx.android.synthetic.main.fragment_control_layout.*
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by i7-4770k on 3/21/2018
 */

class ArticlesFragment : BaseFragment(), ArticlesPresenter.View {

    private var model: CategorySources? = null
    var inputData: ArticlesRequestModel? = null
    var scrollListener: EndlessRecyclerViewScrollListener? = null

    @InjectPresenter
    lateinit var mPresenter: ArticlesPresenter

    companion object {
        val TAG = ArticlesFragment::class.java.simpleName

        fun newInstance(): ArticlesFragment {
            return ArticlesFragment()
        }
    }

    fun setCategory(model: CategorySources) {
        this.model = model
        sourceGroup.isVisible = true
        initSourceModel(model.sourcesList.first())
        domainsSpinnerView.let { initSpinnerView(domainsSpinnerView, model) }
    }

    private fun initSourceModel(model: SourceModel?) {
        inputData?.category = model?.category
        inputData?.country = model?.country
        inputData?.sources = model?.name
    }


    private fun initSpinnerView(spinnerView: Spinner, category: CategorySources) {
        initSpinnerAdapter(category, spinnerView)
        initSpinnerSelections(spinnerView)
    }

    private fun initSpinnerAdapter(category: CategorySources, spinnerView: Spinner) {
        val data = prepareSpinnerDateArr(category)
        val adapter = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, data)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerView.adapter = adapter
    }

    private fun prepareSpinnerDateArr(category: CategorySources): Array<String?> {
        val data = arrayOfNulls<String>(category.sourcesList.size)
        for (i in category.sourcesList.indices) {
            data[i] = category.sourcesList[i].name
        }
        return data
    }

    private fun initSpinnerSelections(spinnerView: Spinner) {
        RxAdapterView.itemSelections<SpinnerAdapter>(spinnerView)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe { integer ->
                    if (model != null) {
                        initSourceModel(model?.sourcesList?.get(integer))
                        reloadArticles()
                    }
                }
    }

    private fun initDateView(view: TextView, ifFromDate: Boolean) {
        val datePicker = CostumeDatePicker()
        datePicker.setSelectedListener(object :CostumeDatePicker.OnDateSelected{
            override fun onSelectedDate(date: Date) {
                selectedDate(view, date, ifFromDate)
            }
        })
        view.setOnClickListener {
            datePicker.show(activity!!.fragmentManager, CostumeDatePicker::class.java.simpleName)
        }
    }

    private fun selectedDate(view: TextView, date: Date, ifFromDate: Boolean) {
        view.text = ConvertDateUtil.convertDateToString(date, ConvertDateUtil.YYYY_MM_DD)
        if (ifFromDate)
            inputData?.from = fromTextView.text.toString()
        else
            inputData?.to = toTextView.text.toString()
        reloadArticles()
    }

    private fun initRxTextView(editText: EditText): Disposable {
        return RxTextView.textChangeEvents(editText)
                .skipInitialValue()
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    inputData?.keywords = editText.text.toString()
                    reloadArticles()
                }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_articles_layout)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initView()
    }

    private fun initView() {
        initSwitchView(whereSwitch)
        initRadioGroupView(sortRadioGroup)
        initDateView(fromTextView, true)
        initDateView(toTextView, false)
        initRxTextView(keyWords)
        initRecyclerView()
    }

    private fun initSwitchView(whereSwitch: Switch) {
        RxCompoundButton.checkedChanges(whereSwitch)
                .skipInitialValue()
                .subscribe { stateChanged ->
                    if (model != null) {
                        initWhere(stateChanged)
                        reloadArticles()
                    } else {
                        needSelectCategory(whereSwitch)
                    }
                }
    }

    private fun initWhere(stateChanged: Boolean) {
        inputData?.where = (if (stateChanged) EVERYTHING else TOP_HEADLINES)
    }

    private fun needSelectCategory(whereSwitch: Switch) {
        whereSwitch.isChecked = false
        showToast(R.string.select_category)
        onPageSelected(0)
    }

    private fun initRadioGroupView(radioGroup: RadioGroup) {
        RxRadioGroup.checkedChanges(radioGroup)
                .skipInitialValue()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe { viewId ->
                    inputData?.sortBy = when (viewId) {
                        R.id.publishedRadioButton -> PUBLISHED
                        R.id.popularityRadioButton -> POPULARITY
                        R.id.relevancyRadioButton -> RELEVANCY
                        else -> PUBLISHED
                    }
                    reloadArticles()
                }
    }

    private fun initRecyclerView() {
        articlesRecyclerView?.apply {
            layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
            scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
                // Scroll to loadMore
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                    mPresenter.loadMoreArticles(page)
                }
            }
            addOnScrollListener(scrollListener)
        }
    }


    override fun displayProgress(inProgress: Boolean) {
        displayProgress(progressBarView, inProgress)
    }

    override fun responseComplete(isSuccess: Boolean, list: List<ArticleModel>) {
        if (isSuccess) articlesRecyclerView?.adapter?.notifyDataSetChanged()
    }

    override fun initAdapter(list: List<ArticleModel>) {
        articlesRecyclerView?.apply {
            val categoryAdapter = ArticlesRecyclerViewAdapter(list as MutableList<ArticleModel>)
            setupItemClick(categoryAdapter)
            adapter = categoryAdapter
        }
    }

    private fun setupItemClick(adapter: ArticlesRecyclerViewAdapter) {
        subscribe = adapter.clickEvent.subscribe { onItemClick(it) }
    }

    private fun onItemClick(clickItem: ClickItemModel) {
        when (clickItem.id) {
            R.id.favoritesImageButton -> mPresenter.initToFavorites(clickItem.model)
            R.id.shareImageButton -> sharedContent(context!!, clickItem.model)
            R.id.parentView -> showToast(clickItem.model.title)
        }
    }

    override fun initInputDate(inputData: ArticlesRequestModel?) {
        this.inputData = inputData
        reloadArticles()
    }

    private fun reloadArticles() {
        mPresenter.reloadArticles()
        scrollListener?.resetState()
        articlesRecyclerView?.apply { adapter?.notifyDataSetChanged() }
    }
}