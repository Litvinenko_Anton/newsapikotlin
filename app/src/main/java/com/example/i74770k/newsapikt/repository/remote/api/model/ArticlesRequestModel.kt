package com.example.i74770k.newsapikt.repository.remote.api.model

import com.example.i74770k.newsapikt.tools.COUNTRY
import com.example.i74770k.newsapikt.tools.EVERYTHING
import com.example.i74770k.newsapikt.tools.PUBLISHED
import com.example.i74770k.newsapikt.tools.TOP_HEADLINES
import java.util.*

class ArticlesRequestModel{

    var where: String = TOP_HEADLINES                   // top-headlines/everything
    var sources: String? = null
    var language: String = Locale.getDefault().language
    var sortBy: String = PUBLISHED                      // publishedAt, popularity, relevancy
    var from: String? = null                            // 2018-01-25
    var to: String? = null                              // 2018-01-25
    var page: Int = 0
    var keywords: String? = null
    var country: String? = COUNTRY // us
    var category: String? = null

    fun isAllArticles(): Boolean {
        return where == EVERYTHING
    }
}