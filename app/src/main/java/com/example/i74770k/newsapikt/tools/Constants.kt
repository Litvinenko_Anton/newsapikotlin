package com.example.i74770k.newsapikt.tools

/**
 * Constants
 */

const val BASE_URL = "https://newsapi.org/v2/"
const val API_KEY = "0003f1c3300148399cabf12edda039de"

const val OK_HTTP_CLIENT_TIMEOUT = 60 //sec
const val PAGE_COUNT = 3
const val HISTORY = "HISTORY"
const val CATEGORY = "CATEGORY"
const val NEWS = "NEWS"
const val UNKNOWN = "UNKNOWN"

const val TOP_HEADLINES = "top-headlines"
const val EVERYTHING = "everything"

const val PUBLISHED = "publishedAd"
const val POPULARITY = "popularity"
const val RELEVANCY = "relevancy"
const val COUNTRY = "us"

