package com.example.i74770k.newsapikt.ui.articles

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.example.i74770k.newsapikt.R
import com.example.i74770k.newsapikt.repository.remote.api.model.ClickItemModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.ArticleModel
import com.example.i74770k.newsapikt.tools.PagingAdapter
import com.jakewharton.rxbinding2.view.RxView
import inflate
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import isVisible
import kotlinx.android.synthetic.main.recyclerview_item_articles_layout.view.*
import load

/**
 * Created by i7-4770k on 4/3/2018
 */
class ArticlesRecyclerViewAdapter(private val mModels: MutableList<ArticleModel>) :
        RecyclerView.Adapter<ArticlesRecyclerViewAdapter.ViewHolder>(), PagingAdapter<ArticleModel> {

    val clickSubject = PublishSubject.create<ClickItemModel>()
    val clickEvent: Observable<ClickItemModel> = clickSubject

    private var isExpired: Boolean = true

    override fun skipListExpired() {
        if (!isExpired) {
            isExpired = true
            skipList()
        }
    }

    override fun skipList() {
        mModels.clear()
        notifyDataSetChanged()
    }

    override fun updateList(newItems: List<ArticleModel>) {
        mModels.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(parent.inflate(R.layout.recyclerview_item_articles_layout))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mModels[position]
        holder.titleTextView?.apply { text = model.title }
        holder.descriptionTextView?.apply { text = model.description }
        holder.dateTextView?.apply { text = model.publishedAt }
        holder.logoImageView.load(model.urlToImage)
        holder.logoImageView.isVisible = model.urlToImage != null
        holder.favoritesImageButton.setImageResource(initFavoritesImageRes(model))
        rxViewClicks(position, model, holder.parentView, holder.shareImageButton, holder.favoritesImageButton)
    }

    private fun initFavoritesImageRes(model: ArticleModel): Int =
            if (model.favorites)
                R.drawable.ic_stars_full_24px
            else
                R.drawable.ic_stars_empty_24px

    override fun getItemCount(): Int = mModels.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val parentView = view.parentView
        val logoImageView = view.logoImageView
        val titleTextView = view.titleTextView
        val descriptionTextView = view.descriptionTextView
        val dateTextView = view.dateTextView
        val favoritesImageButton = view.favoritesImageButton
        val shareImageButton = view.shareImageButton

    }

    private fun rxViewClicks(position: Int, model: ArticleModel, vararg views: View?) {
        for (view in views) {
            view?.let {
                RxView.clicks(view)
                        .map<ClickItemModel> { ClickItemModel(view.id, position, model) }
                        .subscribe(clickSubject)
            }
        }
    }

}