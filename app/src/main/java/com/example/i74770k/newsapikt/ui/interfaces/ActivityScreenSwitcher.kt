package novo.net.novo.ui.interfaces

import android.content.Intent
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import com.example.i74770k.newsapikt.repository.remote.api.model.CategorySources

/**
 * Created by i7-4770k on 25.03.2018
 */
interface ActivityScreenSwitcher {

    fun nextFragment(fragment: Fragment, @IdRes containerViewId: Int, TAG: String)

    fun nextActivity(activityIntent: Intent)

    fun onBackPressed()

    fun onPageSelected(position: Int)

    fun onCategorySelected(category: CategorySources?)


}