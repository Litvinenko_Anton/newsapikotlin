package com.example.i74770k.newsapikt.ui.main

import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewPager.OnPageChangeListener
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.i74770k.newsapikt.presentor.BasePresenter


/**
 * Created by i7-4770k on 3/21/2018
 */
@InjectViewState
class MainPresenter : BasePresenter<MainPresenter.View>() {

    fun initPageAdapter(fm: FragmentManager) {
        val pagerAdapter = SectionsPagerAdapter(fm)
        viewState.initPageAdapter(pagerAdapter)
    }

    /**
     * ViewState
     */

    @StateStrategyType(AddToEndSingleStrategy::class)
    interface View : BasePresenter.View {
        fun initPageAdapter(adapter: SectionsPagerAdapter)
        fun initPageListener(listener: OnPageChangeListener)
        fun onPageSelected(position: Int)

    }

}