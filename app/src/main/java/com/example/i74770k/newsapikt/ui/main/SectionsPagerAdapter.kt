package com.example.i74770k.newsapikt.ui.main

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.i74770k.newsapikt.tools.*
import com.example.i74770k.newsapikt.ui.articles.ArticlesFragment
import com.example.i74770k.newsapikt.ui.category.CategoryFragment
import com.example.i74770k.newsapikt.ui.favorites.FavoritesFragment

class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> CategoryFragment.newInstance()
            1 -> ArticlesFragment.newInstance()
            2 -> FavoritesFragment.newInstance()
            else -> CategoryFragment.newInstance()
        }
    }

    override fun getCount(): Int = PAGE_COUNT

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> HISTORY
            1 -> CATEGORY
            2 -> NEWS
            else -> UNKNOWN
        }
    }
}