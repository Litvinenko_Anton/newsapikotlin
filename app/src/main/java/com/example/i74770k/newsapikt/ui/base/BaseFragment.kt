package com.example.i74770k.newsapikt.ui.base

import android.content.Context
import android.content.Intent
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.arellomobile.mvp.MvpAppCompatFragment
import com.example.i74770k.newsapikt.repository.remote.api.model.CategorySources
import com.example.i74770k.newsapikt.tools.extensions.showToast
import io.reactivex.disposables.Disposable
import isGone
import novo.net.novo.ui.interfaces.ActivityScreenSwitcher

/**
 * Created by i7-4770k on 3/21/2018
 */
abstract class BaseFragment : MvpAppCompatFragment(), ActivityScreenSwitcher {

    protected var subscribe: Disposable? = null
    private var screenSwitcher: ActivityScreenSwitcher? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        screenSwitcher = getActivityControl()
    }

    override fun onDestroy() {
        super.onDestroy()
        subscribe?.dispose()
        screenSwitcher = null
    }

    override fun nextFragment(fragment: Fragment, @IdRes containerViewId: Int, TAG: String) {
        screenSwitcher?.nextFragment(fragment, containerViewId, TAG)
    }

    override fun nextActivity(activityIntent: Intent) {
        screenSwitcher?.nextActivity(activityIntent)
    }

    override fun onBackPressed() {
        screenSwitcher?.onBackPressed()
    }

    override fun onPageSelected(position: Int) {
        screenSwitcher?.onPageSelected(position)
    }

    override fun onDestroyView() {
        hideKeyBoard()
        super.onDestroyView()
    }

    private fun getActivityControl(): ActivityScreenSwitcher = activity as ActivityScreenSwitcher

    private fun hideKeyBoard(): Boolean {
        val view = activity!!.currentFocus
        if (view != null) {
            (activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            return true
        }
        return false
    }

    open fun displayProgress(progressBar: View?, inProgress: Boolean) {
        hideKeyBoard()
        progressBar?.apply { isGone = !inProgress }
    }

    open fun responseComplete(isSuccess: Boolean) {

    }

    open fun responseError(errorMessage: String) {
        showToast(errorMessage)
    }

    override fun onCategorySelected(category: CategorySources?) {
        screenSwitcher?.onCategorySelected(category)
    }
}

