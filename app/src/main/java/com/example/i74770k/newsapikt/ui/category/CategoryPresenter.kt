package com.example.i74770k.newsapikt.ui.category

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.i74770k.newsapikt.presentor.BasePresenter
import com.example.i74770k.newsapikt.repository.remote.api.model.CategorySources
import com.example.i74770k.newsapikt.repository.remote.api.pojo.CategoryModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.SourceModel
import com.example.i74770k.newsapikt.tools.API_KEY

/**
 * Created by i7-4770k on 3/21/2018
 */
@InjectViewState
class CategoryPresenter : BasePresenter<CategoryPresenter.View>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        getCategory()
    }

    private fun getCategory() {
        repository.getCategory(API_KEY)
                .doOnRequest { showProgress("Category") }
                .subscribe({ response -> initCategoryResponse(response) },
                        { throwable -> onError(throwable, "Category") },
                        { onCompleted("Category") })
    }

    private fun initCategoryResponse(response: CategoryModel) {
        if (response.isSuccess())
            viewState.responseComplete(response.isSuccess(), initCategoryList(response))
        else
            viewState.responseError(response.message ?: "No error message")
    }

    private fun initCategoryList(response: CategoryModel): List<CategorySources> {
        val categoryList = ArrayList<CategorySources>()
        for (source in response.sources) {
            addSource(categoryList, source)
        }
        return categoryList
    }

    private fun addSource(categoryList: MutableList<CategorySources>, model: SourceModel): Boolean {
        for (category in categoryList) {
            if (category.category == model.category)
                return category.sourcesList.add(model)
        }
        return categoryList.add(CategorySources(model))
    }


    /**
     * ViewState
     */

    @StateStrategyType(AddToEndSingleStrategy::class)
    interface View : BasePresenter.View {

        fun responseComplete(isSuccess: Boolean, list: List<CategorySources>)

    }

}