package com.example.i74770k.newsapikt.tools

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import com.example.i74770k.newsapikt.R
import java.util.*

/**
 * Created by i7-4770k on 3/26/2018
 */

class CostumeDatePicker : DialogFragment(), DatePickerDialog.OnDateSetListener {

    var callback: OnDateSelected? = null

    fun setSelectedListener(callback: OnDateSelected?) {
        this.callback = callback
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        // Init current date
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        // Create DatePickerDialog with current date
        val datePickerDialog = DatePickerDialog(activity, this, year, month, day)
        datePickerDialog.setTitle(resources.getString(R.string.choose_date))
        return datePickerDialog
    }

    override fun onDateSet(datePicker: android.widget.DatePicker?, year: Int, indexArrayMonth: Int, day: Int) {
        val cal = Calendar.getInstance()
        cal.set(year, indexArrayMonth, day)
        if (callback != null) {
            callback!!.onSelectedDate(cal.time)
        }
    }

    /**
     * Interface
     */
    interface OnDateSelected {
        fun onSelectedDate(date: Date)
    }
}