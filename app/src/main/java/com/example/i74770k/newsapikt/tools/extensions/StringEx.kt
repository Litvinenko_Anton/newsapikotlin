package com.example.i74770k.newsapikt.tools.extensions

import android.content.Context
import android.content.Intent
import com.example.i74770k.newsapikt.repository.remote.api.model.Article
import com.example.i74770k.newsapikt.repository.remote.api.pojo.ArticleModel

fun sharedContent(context: Context, model: ArticleModel) {
    sharedContent(context, "Share ", "${model.title}\n${model.url}")
}

fun sharedContent(context: Context, model: Article) {
    sharedContent(context, "Share ", "${model.title}\n${model.url}")
}

fun sharedContent(context: Context, subject: String, content: String) {
    val shareIntent = Intent(Intent.ACTION_SEND)
    shareIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
    shareIntent.type = "text/plain"
    shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
    shareIntent.putExtra(Intent.EXTRA_TEXT, content)
    val intent = Intent.createChooser(shareIntent, "Share it")
    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
    context.startActivity(intent)
}