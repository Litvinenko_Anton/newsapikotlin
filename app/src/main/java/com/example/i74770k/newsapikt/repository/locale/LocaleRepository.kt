package com.example.i74770k.newsapikt.repository.locale

import com.example.i74770k.newsapikt.repository.remote.api.pojo.ArticleModel
import io.realm.RealmObject

interface LocaleRepository<R : RealmObject> {

    fun saveToRealm(article: ArticleModel)

    fun deleteFromRealm(article: ArticleModel)

    fun deleteFromRealm(`object`: R)

    fun deleteAllFromRealm(objects: List<R>)

    fun containsInRealm(`object`: R): Boolean

    fun containsInRealm(article: ArticleModel): Boolean

    fun closeConnection()

    fun clearRealm()

    fun onDestroy()

    fun addOnResultsChangeListener(listener: LocaleRepositoryRealmImp.OnResultsChangeListener)

    fun removeOnResultsChangeListener(listener: LocaleRepositoryRealmImp.OnResultsChangeListener)

    }