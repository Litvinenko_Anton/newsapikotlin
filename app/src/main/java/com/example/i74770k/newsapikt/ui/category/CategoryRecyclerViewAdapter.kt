package com.example.i74770k.newsapikt.ui.category

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.example.i74770k.newsapikt.R
import com.example.i74770k.newsapikt.repository.remote.api.model.CategorySources
import com.example.i74770k.newsapikt.tools.PagingAdapter
import inflate
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.reciclerview_item_category_layout.view.*

/**
 * Created by i7-4770k on 4/3/2018
 */
class CategoryRecyclerViewAdapter(private val mModels: MutableList<CategorySources>) :
        RecyclerView.Adapter<CategoryRecyclerViewAdapter.ViewHolder>(), PagingAdapter<CategorySources> {

    val clickSubject = PublishSubject.create<CategorySources>()
    val clickEvent: Observable<CategorySources> = clickSubject
    private var isExpired: Boolean = true

    override fun skipListExpired() {
        if (!isExpired) {
            isExpired = true
            skipList()
        }
    }

    override fun skipList() {
        mModels.clear()
        notifyDataSetChanged()
    }

    override fun updateList(newItems: List<CategorySources>) {
        mModels.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(parent.inflate(R.layout.reciclerview_item_category_layout))


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mModels[position]
        holder.titleTextView?.apply { text = model.category }
        holder.nameTextView?.apply { text = model.categoryName }
        holder.countTextView?.apply { text = "(${model.categoryCount()})" }
    }

    override fun getItemCount(): Int = mModels.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titleTextView = view.titleTextView
        val nameTextView = view.nameTextView
        val countTextView = view.countTextView

        init {
            view.setOnClickListener {
                clickSubject.onNext(mModels[layoutPosition])
            }
        }
    }

}