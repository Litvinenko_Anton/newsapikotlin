package com.example.i74770k.newsapikt.repository.remote

import com.example.i74770k.newsapikt.repository.remote.api.model.ArticlesRequestModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.ArticlesModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.CategoryModel
import io.reactivex.Flowable


interface RemoteRepository  {
    fun getCategory(apiKey: String): Flowable<CategoryModel>
    fun getArticles(apiKey: String, request: ArticlesRequestModel): Flowable<ArticlesModel>
    fun getArticlesTop(apiKey: String, request: ArticlesRequestModel): Flowable<ArticlesModel>
}
