package com.example.i74770k.newsapikt.ui.favorites

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.i74770k.newsapikt.presentor.BasePresenter
import com.example.i74770k.newsapikt.repository.locale.LocaleRepositoryRealmImp
import com.example.i74770k.newsapikt.repository.remote.api.model.Article
import io.realm.RealmResults

/**
 * Created by i7-4770k on 3/21/2018
 */
@InjectViewState
class FavoritesPresenter : BasePresenter<FavoritesPresenter.View>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        getFavorites()
    }

    private fun getFavorites() {
        repository.addOnResultsChangeListener(object : LocaleRepositoryRealmImp.OnResultsChangeListener {
            override fun resultsOnChange(articles: RealmResults<Article>) {
                viewState.realmResults(articles.isNotEmpty(), articles.toList())
            }
        })
    }

    fun deleteFromFavorites(model: Article) {
        repository.deleteFromRealm(model)
    }


    /**
     * ViewState
     */

    @StateStrategyType(AddToEndSingleStrategy::class)
    interface View : BasePresenter.View {

        fun realmResults(isSuccess: Boolean, list: List<Article>)

    }

}