package com.example.i74770k.newsapikt.repository.remote.api.pojo

import com.google.gson.annotations.SerializedName
import java.util.*

class ArticlesModel : BaseModel() {

    @SerializedName("totalResults")  val totalResults: Int = 0
    @SerializedName("articles")  val articles = ArrayList<ArticleModel>()

}