package com.example.i74770k.newsapikt.tools.extensions

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

object ConvertDateUtil {

    const val YYYY_MM_DD = "yyyy-MM-dd"
    const val YYYY_MM_DD_HH_MM = "yyyy-MM-dd hh:mm"

    @SuppressLint("SimpleDateFormat")
    fun convertDateToString(date: Date?, outputDateFormat: String = YYYY_MM_DD_HH_MM): String {
        val dateFormat = SimpleDateFormat(outputDateFormat)
        return dateFormat.format(date ?: Date())
    }

}