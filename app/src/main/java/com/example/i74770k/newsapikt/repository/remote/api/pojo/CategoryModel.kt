package com.example.i74770k.newsapikt.repository.remote.api.pojo

import com.google.gson.annotations.SerializedName

class CategoryModel : BaseModel() {

    @SerializedName("sources") val sources: List<SourceModel> = ArrayList()

}