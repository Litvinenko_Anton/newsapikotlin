package com.example.i74770k.newsapikt.ui.category

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.i74770k.newsapikt.R
import com.example.i74770k.newsapikt.repository.remote.api.model.CategorySources
import com.example.i74770k.newsapikt.ui.base.BaseFragment
import inflate
import kotlinx.android.synthetic.main.fragment_category_layout.*

/**
 * Created by i7-4770k on 3/21/2018
 */


class CategoryFragment : BaseFragment(), CategoryPresenter.View {


    @InjectPresenter
    lateinit var mPresenter: CategoryPresenter

    companion object {
        val TAG = CategoryFragment::class.java.simpleName

        fun newInstance(): CategoryFragment {
            return CategoryFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_category_layout)
    }

    private fun initRecyclerView(list: List<CategorySources>) {
        categoryRecyclerView?.apply {
            layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
            val categoryAdapter = CategoryRecyclerViewAdapter(list as MutableList<CategorySources>)
            setupItemClick(categoryAdapter)
            adapter = categoryAdapter
        }
    }

    private fun setupItemClick(adapter: CategoryRecyclerViewAdapter) {
        subscribe = adapter.clickEvent.subscribe { onItemClick(it) }
    }

    private fun onItemClick(model: CategorySources) {
        onCategorySelected(model)
    }

    override fun displayProgress(inProgress: Boolean) {
        displayProgress(progressBar, inProgress)
    }

    override fun responseComplete(isSuccess: Boolean, list: List<CategorySources>) {
        if (isSuccess && list.isNotEmpty()) initRecyclerView(list)
    }
}