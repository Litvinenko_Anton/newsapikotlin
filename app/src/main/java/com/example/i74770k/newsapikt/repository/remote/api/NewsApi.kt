package com.example.i74770k.newsapikt.repository.remote.api

import com.example.i74770k.newsapikt.repository.remote.api.pojo.ArticlesModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.CategoryModel
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


/**
 * Base URL: http://agentplus.dump.dp.ua/api/v1.0/
 */

interface NewsApi {

    @GET("sources")
    fun getCategory(@Query("apiKey") apiKey: String): Flowable<CategoryModel>

    @GET("{where}")
    fun getArticles(
            @Path("where") where: String,       // everything
            @Query("apiKey") apiKey: String,
            @Query("language") language: String?,
            @Query("sources") sources: String?, // name
            @Query("sortBy") sortBy: String?,   // publishedAt, popularity, relevancy
            @Query("from") from: String?,       // 2018-01-25
            @Query("to") to: String?,           // 2018-01-25
            @Query("page") page: Int,
            @Query("q") keywords: String?): Flowable<ArticlesModel>

    @GET("{where}")
    fun getArticlesTop(
            @Path("where") where: String,       // top-headlines
            @Query("apiKey") apiKey: String,
            @Query("language") language: String,
            @Query("country") country: String?, // us
            @Query("category") category: String?,
            @Query("page") page: Int,
            @Query("q") keywords: String?): Flowable<ArticlesModel>

}