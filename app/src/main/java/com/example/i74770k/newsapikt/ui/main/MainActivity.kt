package com.example.i74770k.newsapikt.ui.main

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.i74770k.newsapikt.R
import com.example.i74770k.newsapikt.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), MainPresenter.View {

    @InjectPresenter
    lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainPresenter.initPageAdapter(supportFragmentManager)
    }

    override fun onStart() {
        super.onStart()
        initViewPager()
    }

    private fun initViewPager() {
        if (tabLayout != null && viewPager != null) {
            viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
            tabLayout.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(viewPager))
        }
    }

    override fun initPageAdapter(adapter: SectionsPagerAdapter) {
        viewPager?.apply { this.adapter = adapter }
    }

    override fun initPageListener(listener: ViewPager.OnPageChangeListener) {
        viewPager?.apply { addOnPageChangeListener(listener) }
    }

    override fun onPageSelected(position: Int) {
        viewPager?.setCurrentItem(position, true)
    }

    override fun displayProgress(inProgress: Boolean) {

    }

    override fun responseComplete(isSuccess: Boolean) {

    }

}
