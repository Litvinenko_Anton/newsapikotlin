package com.example.i74770k.newsapikt.repository.remote.api

import android.content.Context
import android.net.ConnectivityManager
import com.example.i74770k.newsapikt.repository.remote.RemoteRepository
import com.example.i74770k.newsapikt.repository.remote.api.model.ArticlesRequestModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.ArticlesModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.CategoryModel
import com.example.i74770k.newsapikt.tools.extensions.logD
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import showToast

/**
 * Created by i7-4770k on 09.05.2018
 */
class RemoteRepositoryRetrofitImpl(private val appContext: Context, val api: NewsApi) : RemoteRepository {

    override fun getCategory(apiKey: String): Flowable<CategoryModel> {
        return getBaseRequestObservable(api.getCategory(apiKey))
    }

    override fun getArticles(apiKey: String, request: ArticlesRequestModel): Flowable<ArticlesModel> {
        return getBaseRequestObservable(
                api.getArticles(
                        request.where,
                        apiKey,
                        request.language,
                        request.sources,
                        request.sortBy,
                        request.from,
                        request.to,
                        request.page,
                        request.keywords
                ))
    }

    override fun getArticlesTop(apiKey: String, request: ArticlesRequestModel): Flowable<ArticlesModel> {
        return getBaseRequestObservable(
                api.getArticlesTop(
                        request.where,
                        apiKey,
                        request.language,
                        request.country,
                        request.category,
                        request.page,
                        request.keywords))
    }

    private fun <T> getBaseRequestObservable(request: Flowable<T>): Flowable<T> {
        isOnline()
        return request.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnError { throwable -> logD("JSON", "BaseRequestObservable Throwable.doOnError() " + throwable.message) }
    }


    private fun isOnline(): Boolean {
        val cm = appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return if (netInfo != null && netInfo.isConnectedOrConnecting) {
            true
        } else {
            appContext.showToast("Check internet connection")
            false
        }
    }
}