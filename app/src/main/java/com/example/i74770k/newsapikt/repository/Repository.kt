package com.example.i74770k.newsapikt.repository

import com.example.i74770k.newsapikt.repository.locale.LocaleRepository
import com.example.i74770k.newsapikt.repository.remote.RemoteRepository
import com.example.i74770k.newsapikt.repository.remote.api.model.Article

interface Repository : RemoteRepository, LocaleRepository<Article>