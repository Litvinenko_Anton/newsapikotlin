package com.example.i74770k.newsapikt.repository

import com.example.i74770k.newsapikt.repository.locale.LocaleRepository
import com.example.i74770k.newsapikt.repository.locale.LocaleRepositoryRealmImp
import com.example.i74770k.newsapikt.repository.remote.RemoteRepository
import com.example.i74770k.newsapikt.repository.remote.api.model.Article
import com.example.i74770k.newsapikt.repository.remote.api.model.ArticlesRequestModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.ArticleModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.ArticlesModel
import com.example.i74770k.newsapikt.repository.remote.api.pojo.CategoryModel
import io.reactivex.Flowable


class RepositoryImp(private val remoteRepository: RemoteRepository,
                    private val localeRepository: LocaleRepository<Article>) : Repository {

    /*REMOTE*/
    override fun getCategory(apiKey: String): Flowable<CategoryModel> =
            remoteRepository.getCategory(apiKey)

    override fun getArticles(apiKey: String, request: ArticlesRequestModel): Flowable<ArticlesModel> =
            remoteRepository.getArticles(apiKey, request)

    override fun getArticlesTop(apiKey: String, request: ArticlesRequestModel): Flowable<ArticlesModel> =
            remoteRepository.getArticlesTop(apiKey, request)


    /*LOCAL*/
    override fun saveToRealm(article: ArticleModel) = localeRepository.saveToRealm(article)

    override fun deleteFromRealm(`object`: Article) = localeRepository.deleteFromRealm(`object`)

    override fun deleteFromRealm(article: ArticleModel) = localeRepository.deleteFromRealm(article)

    override fun deleteAllFromRealm(objects: List<Article>) = localeRepository.deleteAllFromRealm(objects)

    override fun containsInRealm(`object`: Article): Boolean = localeRepository.containsInRealm(`object`)

    override fun containsInRealm(article: ArticleModel): Boolean = localeRepository.containsInRealm(article)

    override fun closeConnection() = localeRepository.closeConnection()

    override fun clearRealm() = localeRepository.clearRealm()

    override fun onDestroy() = localeRepository.onDestroy()

    override fun addOnResultsChangeListener(listener: LocaleRepositoryRealmImp.OnResultsChangeListener) = localeRepository.addOnResultsChangeListener(listener)

    override fun removeOnResultsChangeListener(listener: LocaleRepositoryRealmImp.OnResultsChangeListener) = localeRepository.removeOnResultsChangeListener(listener)
}
