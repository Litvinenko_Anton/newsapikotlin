package novo.net.novo.di

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationClass

@Qualifier
annotation class ApplicationContext

@Qualifier
annotation class ActivityContext