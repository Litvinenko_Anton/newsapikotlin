package com.example.i74770k.newsapikt.repository.locale

import com.example.i74770k.newsapikt.repository.remote.api.model.Article
import com.example.i74770k.newsapikt.repository.remote.api.pojo.ArticleModel
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmResults
import io.realm.kotlin.where

class LocaleRepositoryRealmImp(private val realm: Realm) : LocaleRepository<Article>, RealmChangeListener<RealmResults<Article>> {

    lateinit var articlesRealmResults: RealmResults<Article>
    private var listeners: MutableSet<OnResultsChangeListener> = HashSet()

    init {
        findAll()
    }

    private fun realmTransaction(function: (realm: Realm) -> Unit) {
        val realm = Realm.getDefaultInstance()
        try {
            realm.beginTransaction()
            function(realm)
            realm.commitTransaction()
        } catch (e: Exception) {
            realm.cancelTransaction()
        }
        realm.close()
    }

    private fun findAll(): RealmResults<Article> {
        articlesRealmResults = realm.where<Article>().findAll()
        articlesRealmResults.addChangeListener(this)
        return articlesRealmResults
    }

    override fun addOnResultsChangeListener(listener: OnResultsChangeListener) {
        listeners.add(listener)
        listener.resultsOnChange(articlesRealmResults)
    }

    override fun removeOnResultsChangeListener(listener: OnResultsChangeListener) {
        listeners.remove(listener)
    }

    override fun saveToRealm(article: ArticleModel) {
        val realmObject: Article = getRealmObjectFromKotlin(article)
        realmTransaction { it.copyToRealmOrUpdate(realmObject) }
    }

    private fun getRealmObjectFromKotlin(article: ArticleModel): Article {
        article.favorites = true
        val realmObject = Article()
        realmObject.favorites = article.favorites
        realmObject.author = article.author
        realmObject.title = article.title
        realmObject.description = article.description
        realmObject.url = article.url
        realmObject.urlToImage = article.urlToImage
        realmObject.publishedAt = article.publishedAt
        realmObject.id = realmObject.hashCode()
        return realmObject
    }


    override fun containsInRealm(article: ArticleModel): Boolean {
        return containsInRealm(getRealmObjectFromKotlin(article))
    }

    override fun containsInRealm(`object`: Article): Boolean {
        val id = `object`.id
        var result: Article? = null

        realmTransaction {
            result = it.where<Article>()
                    .equalTo("id", id)
                    .findFirst()
        }
        return result != null
    }


    override fun deleteFromRealm(`object`: Article) {
        realmTransaction { `object`.deleteFromRealm() }
    }

    override fun deleteFromRealm(article: ArticleModel) {
        val id = getRealmObjectFromKotlin(article).id
        for (realmObject in articlesRealmResults) {
            if (realmObject.id == id) {
                article.favorites = false
                deleteFromRealm(realmObject)
            }
        }
    }

    override fun deleteAllFromRealm(objects: List<Article>) {
        realmTransaction {
            for (`object` in objects) {
                `object`.deleteFromRealm()
            }
        }
    }

    override fun onChange(articles: RealmResults<Article>) {
        for (listener in listeners) {
            listener.resultsOnChange(articles)
        }
    }

    override fun closeConnection() {
        realm.close()
    }

    override fun onDestroy() {
        listeners.clear()
        articlesRealmResults.removeChangeListener(this)
    }

    override fun clearRealm() {
        realm.executeTransaction { articlesRealmResults.deleteAllFromRealm() }
    }

    interface OnResultsChangeListener {
        fun resultsOnChange(articles: RealmResults<Article>)
    }
}